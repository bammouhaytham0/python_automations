import boto3
import schedule
from operator import itemgetter
ec2_client = boto3.client("ec2")


def clean():
    volumes = ec2_client.describe_volumes().get('Volumes')
    for volume in volumes:
        snapshots = ec2_client.describe_snapshots(OwnerIds=['self'], Filters=[{'Name': 'volume-id', 'Values': [volume['VolumeId']]}])
        sorted_by_dates = sorted(snapshots['Snapshots'], key=itemgetter('StartTime'), reverse=True)

        for snap in sorted_by_dates[2:]:
            ec2_client.delete_snapshot(SnapshotId=snap["SnapshotId"])


schedule.every().week.do(clean)

while True:
    schedule.run_pending()
