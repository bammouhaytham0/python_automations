import requests
import schedule
import paramiko
import smtplib
import os

EMAIL = os.environ.get("email")
PWD = os.environ.get("pwd")
id_rsa = os.environ.get('id_rsa')
url = "http://20.214.104.153:8080/login"
cmd = "docker start $(docker ps -a -q) && docker exec -it -u 0 $(docker ps -q) chmod 666 /var/run/docker.sock"
def send_email(msg):
    with smtplib.SMTP('smtp.gmail.com', 587) as smtp:
        print("email sent")
        smtp.starttls()
        smtp.ehlo()
        smtp.login(EMAIL, PWD)
        smtp.sendmail(EMAIL, EMAIL, msg)


def restartApp():
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy)
    ssh.connect("20.214.104.153", username="root", key_filename=id_rsa)
    stdin, stdout, stderr = ssh.exec_command(cmd)
    print(stdout.readlines())
    print(stderr.readlines())
    ssh.close()
    print("application restarted")


def check_website():
    try:
        response = requests.get(url)
        if response.status_code == 200:
            print("application is up")
        else :
            print("application down fixe it")
            # sending an email
            send_email(f"Subject: SITE DOWN\n application failed with status code {response.status_code} Fix the issue")
            # restart the application
            restartApp()
    except Exception as exp:
        print(f"Connexion error happend {exp}")
        send_email("Subject: SERVER DOWN\nSERVER DOWN")
        # restart the application
        restartApp()



schedule.every(10).seconds.do(check_website)

while True:
    schedule.run_pending()