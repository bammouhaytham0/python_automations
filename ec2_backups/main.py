import boto3
import schedule

ec2_client = boto3.client("ec2")


def create_backups():
    volumes = ec2_client.describe_volumes().get('Volumes')
    for volume in volumes:
        snapshot = ec2_client.create_snapshot(VolumeId=volume['VolumeId'])
        print(snapshot)


schedule.every().day.do(create_backups)

while True:
    schedule.run_pending()

