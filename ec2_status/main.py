import boto3
import schedule

ec2_client = boto3.client('ec2')
ec2_resource = boto3.resource('ec2')


def check_instance_status():
    statuses = ec2_client.describe_instance_status()
    for status in statuses['InstanceStatuses']:
        inst_status = status["InstanceStatus"]["Status"]
        sys_status = status["SystemStatus"]["Status"]
        print(f"instance {status['InstanceId']} instance status is : {inst_status} , system status : {sys_status}")
    print("#"*20)


schedule.every(5).seconds.do(check_instance_status)

while True:
    schedule.run_pending()
