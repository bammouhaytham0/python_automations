import boto3
from operator import itemgetter

ec2_client = boto3.client('ec2')
ec2_resource = boto3.resource('ec2')


def restore_instance(instance_id):
    volumes = ec2_client.describe_volumes(Filters=[{'Name': 'attachment.instance-id', 'Values': [instance_id]}])
    volume = volumes.get('Volumes')[0]
    snapshots = ec2_client.describe_snapshots(OwnerIds=['self'], Filters=[{'Name': 'volume-id', 'Values': [volume['VolumeId']]}])
    latest_snapshot = sorted(snapshots['Snapshots'], key=itemgetter('StartTime'), reverse=True)[0]
    newVolume = ec2_client.create_volume(SnapshotId=latest_snapshot['SnapshotId'], AvailabilityZone=volume["AvailabilityZone"])
    while True:
        vol = ec2_resource.Volume(newVolume['VolumeId'])
        print(vol.state)
        if vol.state == 'available': break
    ec2_resource.Instance(instance_id).attach_volume(VolumeId=newVolume['VolumeId'], Device='/dev/xvdb')
